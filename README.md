# Projekt: Aplikacja do zarządzania zadaniami (ToDo List). 

Aplikacja zawiera rozróżnienie użytkowników na role Użytkownik i Admin. 

Dla użytkownika umożliwia:
- Wyświetlania listy swoich zadań 
- Dodawania nowego zadania 
- Edycję zadania 
- Usunięcia zadania 
- Oznaczenie zadania jako wykonane lub niewykonane 

Dla administratora umożliwia:
- Wyświetlenie listy wszystkich zadań użytkowników 
- Wyświetlanie listy zadań konkretnego użytkownika 
- Wyświetlenie listy użytkowników z informacją o ilości zadań użytkownika 
- Dodanie nowego użytkownika 
- Edycję użytkownika 
- Zablokowanie lub odblokowanie konta użytkownika 


Technologie: Java, Spring Boot, Thymeleaf

Aplikacja: http://localhost:8080/
Baza danych H2: http://localhost:8080/h2-console/

## Zrzuty ekranu

### Panel logowania
![Alt text](docs/readme/1.png)

### Strona główna
![Alt text](docs/readme/2.png)

### Panel administratora - lista wszystkich użytkowników
![Alt text](docs/readme/3.png)

### Panel administratora - lista wszystkich zadań
![Alt text](docs/readme/4.png)

### Panel administratora - lista zadań pojedynczego użytkownika
![Alt text](docs/readme/5.png)

### Panel administratora - formularz dodawania nowego użytkownika
![Alt text](docs/readme/6.png)

### Panel administratora - formularz edycji użytkownika
![Alt text](docs/readme/7.png)

### Strona błędu 403 - brak uprawnień do wyświetlenia strony
![Alt text](docs/readme/8.png)