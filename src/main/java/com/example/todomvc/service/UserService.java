package com.example.todomvc.service;

import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.exceptions.exception.SelfBlockedException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;
import com.example.todomvc.model.User;

import java.util.List;

public interface UserService {

    User getUserById(Long userId);
    User getUserByUsername(String username);

    List<User> getAllUsers();

    void addUser(User user) throws DataSaveException, UserAlreadyExistsException, UserBlockedException;

    void updateUser(Long userId, User user) throws DataSaveException, UserAlreadyExistsException, UserBlockedException;

    void toggleUserBlock(Long userId) throws DataSaveException, UserBlockedException, SelfBlockedException;
}
