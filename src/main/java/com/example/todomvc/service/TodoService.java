package com.example.todomvc.service;

import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.model.TodoItem;
import com.example.todomvc.model.User;

import java.util.List;

public interface TodoService {

    List<TodoItem> getAllTodoItems();
    TodoItem getTodoItemById(Long todoId);
    void toggleComplete(Long todoId) throws DataSaveException;

    void addTask(TodoItem todoItem, User user) throws DataSaveException;
    void updateItem(Long todoId, TodoItem todoItem) throws DataSaveException;
    void deleteTodoItem(Long todoId) throws DataSaveException;
}
