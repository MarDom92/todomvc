package com.example.todomvc.service.impl;

import com.example.todomvc.exceptions.ExceptionFactory;
import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;
import com.example.todomvc.model.TodoItem;
import com.example.todomvc.model.User;
import com.example.todomvc.repository.TodoItemRepository;
import com.example.todomvc.service.TodoService;
import com.example.todomvc.validation.UserValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TodoServiceImpl implements TodoService {

    private final TodoItemRepository todoItemRepository;

    @Override
    public List<TodoItem> getAllTodoItems() {

        return todoItemRepository.findAll();
    }

    @Override
    public TodoItem getTodoItemById(Long todoId) {

        return todoItemRepository.findById(todoId)
                .orElseThrow(() -> ExceptionFactory.createEntityNotFoundException(todoId));
    }

    @Override
    public void toggleComplete(Long todoId) throws DataSaveException {

        TodoItem todoItem = getTodoItemById(todoId);
        todoItem.setCompleted(!todoItem.isCompleted());

        try {
            todoItemRepository.save(todoItem);
        } catch (DataIntegrityViolationException ex) {
            throw ExceptionFactory.createDataSaveException(ex);
        }
    }

    @Override
    public void addTask(TodoItem todoItem, User user) throws DataSaveException {
        saveTask(todoItem, user);
    }


    private void saveTask(TodoItem todoItem, User user) throws DataSaveException {
        todoItem.setUser(user);
        todoItem.setCreatedDate(LocalDateTime.now().minusHours(10));
        todoItem.setModifiedDate(LocalDateTime.now().plusHours(1));
        todoItem.setCompleted(false);
        try {
            todoItemRepository.save(todoItem);
        } catch (DataIntegrityViolationException ex) {
            throw ExceptionFactory.createDataSaveException(ex);
        }
    }
    @Override
    public void updateItem(Long itemId, TodoItem todoItem) throws DataSaveException {
       TodoItem existingItem = todoItemRepository.findById(itemId)
                .orElseThrow(() -> ExceptionFactory.createEntityNotFoundException(itemId));

        existingItem.setTitle(todoItem.getTitle());
        existingItem.setDescription(todoItem.getDescription());
        saveItem(todoItem);
    }

    private void saveItem(TodoItem todoItem) throws DataSaveException {
        try {
            todoItemRepository.save(todoItem);
        } catch (DataIntegrityViolationException ex) {
            throw ExceptionFactory.createDataSaveException(ex);
        }
    }
    @Override
    public void deleteTodoItem(Long todoId) throws DataSaveException {

        try {
            todoItemRepository.deleteById(todoId);
        } catch (DataIntegrityViolationException ex) {
            throw ExceptionFactory.createDataSaveException(ex);
        }
    }
}
