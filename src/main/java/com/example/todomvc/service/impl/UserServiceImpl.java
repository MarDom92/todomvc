package com.example.todomvc.service.impl;

import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.exceptions.ExceptionFactory;
import com.example.todomvc.exceptions.exception.SelfBlockedException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;
import com.example.todomvc.model.User;
import com.example.todomvc.repository.UserRepository;
import com.example.todomvc.service.UserService;
import com.example.todomvc.validation.UserValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public User getUserById(Long userId) {

        UserValidator.validateId(userId);

        return userRepository.findById(userId)
                .orElseThrow(() -> ExceptionFactory.createEntityNotFoundException(userId));
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> ExceptionFactory.createUsernameNotFoundException(username));
    }

    @Override
    public List<User> getAllUsers() {

        return userRepository.findAll();
    }

    @Override
    public void addUser(User user) throws DataSaveException, UserAlreadyExistsException, UserBlockedException {

        boolean userIsEnabled = isCurrentUserEnabled();
        UserValidator.validateUserEnabled(userIsEnabled);

        String username = user.getUsername();
        boolean existByUsername = userRepository.existsByUsername(username);
        UserValidator.validateUsername(username, existByUsername);

        saveUser(user);
    }

    private boolean isCurrentUserEnabled() {

        String loggedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findIsEnabledByUsername(loggedUsername);
    }

    private void saveUser(User user) throws DataSaveException {

        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException ex) {
            throw ExceptionFactory.createDataSaveException(ex);
        }
    }

    @Override
    public void updateUser(Long userId, User user) throws DataSaveException, UserAlreadyExistsException, UserBlockedException {

        boolean userIsEnabled = isCurrentUserEnabled();
        UserValidator.validateUserEnabled(userIsEnabled);

        UserValidator.validateId(userId);
        String username = user.getUsername();
        boolean existByUsername = userRepository.existsByUsername(username);
        UserValidator.validateUsername(username, existByUsername);

        User existingUser = userRepository.findById(userId)
                .orElseThrow(() -> ExceptionFactory.createEntityNotFoundException(userId));

        existingUser.setUsername(user.getUsername());
        existingUser.setRole(user.getRole());
        existingUser.setEnabled(user.isEnabled());

        saveUser(user);
    }

    @Override
    public void toggleUserBlock(Long userId) throws DataSaveException, UserBlockedException, SelfBlockedException {

        boolean userIsEnabled = isCurrentUserEnabled();
        UserValidator.validateUserEnabled(userIsEnabled);

        UserValidator.validateId(userId);
        User user = userRepository.findById(userId)
                .orElseThrow(() -> ExceptionFactory.createEntityNotFoundException(userId));

        String loggedUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        UserValidator.validateCurrentUserIsBlocked(loggedUsername, user.getUsername());

        user.setEnabled(!user.isEnabled());

        saveUser(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {

        return userRepository.findByUsername(username)
                .orElseThrow(() -> ExceptionFactory.createUsernameNotFoundException(username));
    }
}
