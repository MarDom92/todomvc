package com.example.todomvc.exceptions.exception;

public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(String message) {

        super(message);
    }
}

