package com.example.todomvc.exceptions.exception;

public class SelfBlockedException extends Exception {

    public SelfBlockedException(String message) {

        super(message);
    }
}
