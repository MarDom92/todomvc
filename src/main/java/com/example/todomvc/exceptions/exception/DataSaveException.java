package com.example.todomvc.exceptions.exception;

public class DataSaveException extends Exception {

    public DataSaveException(String message, Throwable cause) {

        super(message, cause);
    }
}

