package com.example.todomvc.exceptions.exception;

public class UserBlockedException extends Exception {

    public UserBlockedException(String message) {

        super(message);
    }
}
