package com.example.todomvc.exceptions;

import com.example.todomvc.exceptions.exception.DataSaveException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Objects;

public class ExceptionFactory {

    private ExceptionFactory() {

        throw new IllegalStateException("ExceptionFactory class");
    }

    public static EntityNotFoundException createEntityNotFoundException(Long id) {

        if (Objects.nonNull(id)) {
            return new EntityNotFoundException("User not found with id: " + id);
        } else {
            return new EntityNotFoundException("User not found");
        }
    }

    public static UsernameNotFoundException createUsernameNotFoundException(String username) {

        if (Objects.nonNull(username)) {
            return new UsernameNotFoundException("User not found with username: " + username);
        } else {
            return new UsernameNotFoundException("User not found");
        }
    }

    public static DataSaveException createDataSaveException(DataIntegrityViolationException ex) {

        return new DataSaveException("Failed to save user", ex);
    }
}
