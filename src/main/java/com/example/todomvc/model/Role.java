package com.example.todomvc.model;

public enum Role {

    USER,
    ADMIN
}
