package com.example.todomvc.controller;

import com.example.todomvc.constants.ErrorMessageConstants;
import com.example.todomvc.constants.UrlConstants;
import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.exceptions.exception.SelfBlockedException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;
import com.example.todomvc.model.Role;
import com.example.todomvc.model.TodoItem;
import com.example.todomvc.model.User;
import com.example.todomvc.service.TodoService;
import com.example.todomvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

import static com.example.todomvc.constants.AttributeNamesConstants.ERROR_MESSAGE_ATTR;
import static com.example.todomvc.constants.AttributeNamesConstants.ROLES_ATTR;
import static com.example.todomvc.constants.AttributeNamesConstants.TODO_ITEMS_ATTR;
import static com.example.todomvc.constants.AttributeNamesConstants.USERS_ATTR;
import static com.example.todomvc.constants.AttributeNamesConstants.USER_ATTR;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final TodoService todoService;
    private final UserService userService;

    @GetMapping("/todos")
    public String showAllTodoItems(Model model) {

        List<TodoItem> todoItems = todoService.getAllTodoItems();
        model.addAttribute(TODO_ITEMS_ATTR, todoItems);

        return UrlConstants.TH_ADMIN_ALL_TODO_LIST;
    }

    @GetMapping("/todos/{userId}")
    public String showTodoItemsForUser(@PathVariable Long userId, Model model) {

        User user = userService.getUserById(userId);
        List<TodoItem> todoItems = user.getTodoItems();
        model.addAttribute(USER_ATTR, user);
        model.addAttribute(TODO_ITEMS_ATTR, todoItems);

        return UrlConstants.TH_ADMIN_USER_TODO_LIST;
    }

    @GetMapping("/users")
    public String showAllUsers(Model model) {

        List<User> users = userService.getAllUsers();
        model.addAttribute(USERS_ATTR, users);

        return UrlConstants.TH_ADMIN_USER_LIST;
    }

    @GetMapping("/users/add-form")
    public String showAddUserForm(Model model) {

        List<Role> roles = List.of(Role.values());
        model.addAttribute(ROLES_ATTR, roles);
        model.addAttribute(USER_ATTR, new User());

        return UrlConstants.TH_ADMIN_ADD_USER_FORM;
    }

    @PostMapping("/users")
    public String addUser(@ModelAttribute(USER_ATTR) User user, RedirectAttributes redirectAttributes) {

        try {
            userService.addUser(user);
            return UrlConstants.REDIRECT + UrlConstants.ADMIN_USERS;
        } catch (UserAlreadyExistsException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USERNAME_ALREADY_EXIST, UrlConstants.ADMIN_USERS_ADD_FORM);
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.ADMIN_USERS_ADD_FORM);
        } catch (UserBlockedException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USER_IS_BLOCKED, UrlConstants.ADMIN_USERS_ADD_FORM);
        }
    }

    private String handleException(RedirectAttributes redirectAttributes, String errorMessage, String redirectUrl) {

        redirectAttributes.addFlashAttribute(ERROR_MESSAGE_ATTR, errorMessage);
        return UrlConstants.REDIRECT + redirectUrl;
    }

    @GetMapping("/users/edit/{userId}")
    public String showEditUserForm(@PathVariable Long userId, Model model) {

        User user = userService.getUserById(userId);
        List<Role> roles = List.of(Role.values());
        model.addAttribute(USER_ATTR, user);
        model.addAttribute(ROLES_ATTR, roles);

        return UrlConstants.TH_ADMIN_EDIT_USER_FORM;
    }

    @PostMapping("/users/edit/{userId}")
    public String updateUser(@PathVariable Long userId, @ModelAttribute(USER_ATTR) User user, RedirectAttributes redirectAttributes) {

        try {
            userService.updateUser(userId, user);
            return UrlConstants.REDIRECT + UrlConstants.ADMIN_USERS;
        } catch (UserAlreadyExistsException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USERNAME_ALREADY_EXIST, UrlConstants.ADMIN_USERS_REFRESH_EDIT_FORM + userId);
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.ADMIN_USERS_REFRESH_EDIT_FORM + userId);
        } catch (UserBlockedException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USER_IS_BLOCKED, UrlConstants.ADMIN_USERS_REFRESH_EDIT_FORM + userId);
        }
    }

    @PostMapping("/users/toggle-block/{userId}")
    public String toggleUserBlock(@PathVariable Long userId, RedirectAttributes redirectAttributes) {

        try {
            userService.toggleUserBlock(userId);
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.ADMIN_USERS);
        } catch (UserBlockedException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USER_IS_BLOCKED, UrlConstants.ADMIN_USERS);
        } catch (SelfBlockedException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.USER_CANNOT_BE_BLOCKED, UrlConstants.ADMIN_USERS);
        }
        return UrlConstants.REDIRECT + UrlConstants.ADMIN_USERS;
    }
}
