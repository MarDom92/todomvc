package com.example.todomvc.controller;

import com.example.todomvc.constants.ErrorMessageConstants;
import com.example.todomvc.constants.UrlConstants;
import com.example.todomvc.exceptions.exception.DataSaveException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;
import com.example.todomvc.model.Role;
import com.example.todomvc.model.TodoItem;
import com.example.todomvc.model.User;
import com.example.todomvc.repository.TodoItemRepository;
import com.example.todomvc.repository.UserRepository;
import com.example.todomvc.service.TodoService;
import com.example.todomvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static com.example.todomvc.constants.AttributeNamesConstants.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class TodoItemController {

    private final TodoService todoService;
    private final UserService userService;
    private final TodoItemRepository todoItemRepository;

    @GetMapping("/todo-list")
    public String showTodoItemsForUser(Model model, Principal principal) {
        String username = principal.getName();
        User user = userService.getUserByUsername(username);
        List<TodoItem> todoItems = user.getTodoItems();
        model.addAttribute(USER_ATTR, user);
        model.addAttribute(TODO_ITEMS_ATTR, todoItems);

        return UrlConstants.USER_TODO_LIST;
    }

    @GetMapping("/add-task-form/{userId}")
    public String showAddTaskForm(@PathVariable Long userId, Model model) {
        User user = userService.getUserById(userId);
        model.addAttribute(USER_ATTR, user);
        model.addAttribute(TODO_ITEMS_ATTR, new TodoItem());

        return UrlConstants.USER_ADD_TASK_FORM;
    }

    @PostMapping("/add-task-form/{userId}")
    public String addTask(@PathVariable Long userId, @ModelAttribute(TODO_ITEMS_ATTR) TodoItem todoItem, RedirectAttributes redirectAttributes) {
        User user = userService.getUserById(userId);
        try {
            todoService.addTask(todoItem, user);
            return UrlConstants.REDIRECT + UrlConstants.USER_TODO_LIST;
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.USER_ADD_TASK_FORM);}
    }

    private String handleException(RedirectAttributes redirectAttributes, String errorMessage, String redirectUrl) {

        redirectAttributes.addFlashAttribute(ERROR_MESSAGE_ATTR, errorMessage);
        return UrlConstants.REDIRECT + redirectUrl;
    }

    @PostMapping("/todo-list/toggle/{todoId}")
    public String toggleComplete(@PathVariable Long todoId, RedirectAttributes redirectAttributes) {

        try {
            todoService.toggleComplete(todoId);
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.ADMIN_USERS);
        }
        return UrlConstants.REDIRECT + UrlConstants.USER_TODO_LIST;
    }

    @GetMapping("/edit-task-form/{todoId}")
    public String showEditTaskForm(@PathVariable Long todoId, Model model) {
        TodoItem todoItem = todoService.getTodoItemById(todoId);
        model.addAttribute("todoItem", todoItem);

        return "/user/edit-task-form";
    }

    @PostMapping("/edit-task-form/{todoId}")
    public String updateTask(@PathVariable Long todoId, @ModelAttribute(TODO_ITEMS_ATTR) TodoItem todoItem, RedirectAttributes redirectAttributes) {

        try {
            todoService.updateItem(todoId, todoItem);
            return UrlConstants.REDIRECT + UrlConstants.USER_TODO_LIST;
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, "/user/edit-task-form" + todoId);
        }
    }
    @PostMapping("/todo-list/delete/{todoId}")
    public String deleteTodo(@PathVariable Long todoId, RedirectAttributes redirectAttributes) {

        try {
            todoService.getTodoItemById(todoId);
            todoService.deleteTodoItem(todoId);
            return UrlConstants.REDIRECT + UrlConstants.USER_TODO_LIST;
        } catch (DataSaveException e) {
            return handleException(redirectAttributes, ErrorMessageConstants.DATA_SAVE_ERROR, UrlConstants.USER_TODO_LIST);
        }
    }
}