package com.example.todomvc.repository;

import com.example.todomvc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

    @Query("SELECT u.isEnabled FROM User u WHERE u.username = ?1")
    boolean findIsEnabledByUsername(String username);
}
