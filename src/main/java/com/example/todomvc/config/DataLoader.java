package com.example.todomvc.config;

import com.example.todomvc.model.Role;
import com.example.todomvc.model.TodoItem;
import com.example.todomvc.model.User;
import com.example.todomvc.repository.TodoItemRepository;
import com.example.todomvc.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DataLoader {

    private final TodoItemRepository todoItemRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {

        TodoItem todoItem1 = TodoItem.builder()
                .id(1L)
                .title("Lista zakupów")
                .description("Mleko, chleb, pomidory, woda")
                .createdDate(LocalDateTime.now().minusHours(2))
                .modifiedDate(LocalDateTime.now())
                .isCompleted(true)
                .build();

        TodoItem todoItem2 = TodoItem.builder()
                .id(2L)
                .title("Spotkanie")
                .description("Kawiarnia na Nowym Świecie")
                .createdDate(LocalDateTime.now().minusHours(1))
                .modifiedDate(LocalDateTime.now().plusHours(1))
                .isCompleted(false)
                .build();

        TodoItem todoItem3 = TodoItem.builder()
                .id(3L)
                .title("Praca domowa")
                .description("Zadania z matematyki")
                .createdDate(LocalDateTime.now().minusHours(10))
                .modifiedDate(LocalDateTime.now().plusHours(6))
                .isCompleted(true)
                .build();

        todoItemRepository.saveAll(
                List.of(todoItem1, todoItem2, todoItem3)
        );

        User user1 = User.builder()
                .id(1L)
                .username("Jan123")
                .password(passwordEncoder.encode("user1"))
                .isEnabled(true)
                .role(Role.USER)
                .todoItems(List.of(todoItem1))
                .build();

        User user2 = User.builder()
                .id(2L)
                .username("Piotr987")
                .password(passwordEncoder.encode("user2"))
                .isEnabled(true)
                .role(Role.ADMIN)
                .todoItems(List.of(todoItem2, todoItem3))
                .build();

        todoItem1.setUser(user1);
        todoItem2.setUser(user2);
        todoItem3.setUser(user2);

        userRepository.saveAll(
                List.of(user1, user2)
        );
    }
}
