package com.example.todomvc.validation;

import com.example.todomvc.exceptions.exception.SelfBlockedException;
import com.example.todomvc.exceptions.exception.UserAlreadyExistsException;
import com.example.todomvc.exceptions.exception.UserBlockedException;

public class UserValidator {

    private UserValidator() {

        throw new IllegalStateException("UserValidator class");
    }

    public static void validateId(long id) {

        if (id < 1) {
            throw new IllegalArgumentException("Invalid user id value: " + id);
        }
    }

    public static void validateUsername(String username, boolean existByUsername) throws UserAlreadyExistsException {

        if (existByUsername) {
            throw new UserAlreadyExistsException("User with username '" + username + "' already exists.");
        }
    }

    public static void validateUserEnabled(boolean isEnabled) throws UserBlockedException {

        if (!isEnabled) {
            throw new UserBlockedException("User is blocked and cannot perform operations.");
        }
    }

    public static void validateCurrentUserIsBlocked(String loggedUsername, String username) throws SelfBlockedException {

        if (loggedUsername.equals(username)) {
            throw new SelfBlockedException("The currently logged in user cannot be blocked.");
        }
    }
}
