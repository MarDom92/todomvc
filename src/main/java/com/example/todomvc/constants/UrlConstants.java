package com.example.todomvc.constants;

public class UrlConstants {

    public static final String REDIRECT = "redirect:";
    public static final String TH_ADMIN_ADD_USER_FORM = "/admin/add-user-form";
    public static final String TH_ADMIN_ALL_TODO_LIST = "/admin/all-todo-list";
    public static final String TH_ADMIN_EDIT_USER_FORM = "/admin/edit-user-form";
    public static final String TH_ADMIN_USER_LIST = "/admin/user-list";
    public static final String TH_ADMIN_USER_TODO_LIST = "/admin/user-todo-list";

    public static final String ADMIN_USERS = "/admin/users";
    public static final String ADMIN_USERS_ADD_FORM = "/admin/users/add-form";
    public static final String ADMIN_USERS_REFRESH_EDIT_FORM = "/admin/users/edit/";

    public static final String USER_TODO_LIST = "/user/todo-list";
    public static final String USER_ADD_TASK_FORM = "/user/add-task-form";

    private UrlConstants() {

        throw new IllegalStateException("UrlConstants - Constants class");
    }
}
