package com.example.todomvc.constants;

public class ErrorMessageConstants {

    public static final String USERNAME_ALREADY_EXIST = "Użytkownik o podanej nazwie już istnieje. Proszę wybrać inną.";
    public static final String DATA_SAVE_ERROR = "Wystąpił problem podczas zapisywania danych. Prosimy spróbować ponownie.";
    public static final String USER_IS_BLOCKED = "Użytkownik nie może wykonywać operacji, bo został zablokowany.";
    public static final String USER_CANNOT_BE_BLOCKED = "Nie można zablokować aktualnie zalogowanego użytkownika.";

    private ErrorMessageConstants() {

        throw new IllegalStateException("ErrorMessage - Utility class");
    }
}
