package com.example.todomvc.constants;

public class AttributeNamesConstants {

    public static final String USER_ATTR = "user";
    public static final String USERS_ATTR = "users";
    public static final String ROLES_ATTR = "roles";
    public static final String TODO_ITEMS_ATTR = "todoItems";
    public static final String ERROR_MESSAGE_ATTR = "errorMessage";

    private AttributeNamesConstants() {

        throw new IllegalStateException("AttributeNameConstants - Constants class");
    }
}
